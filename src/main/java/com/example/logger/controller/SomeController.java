package com.example.logger.controller;

import com.example.logger.service.SomeService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SomeController {

    private final SomeService someService;

    public SomeController(SomeService someService) {
        this.someService = someService;
    }

    @GetMapping(value = "/log", produces = MediaType.APPLICATION_JSON_VALUE)
    public void log() {
        someService.log();
    }

}
