package com.example.logger.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ErrorEvent {
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime event_at;
    private String level;
    private String service;
    private String server;
    private String version;
    private String request_url;
    private String request_body;

    private String error_class;
    private String error_method;
    private String error_file_name;
    private int error_line;

    private String http_method;
    private String path;
    private String user_agent;
    private String referrer;

    private String message;
    private String stack_trace;
}
