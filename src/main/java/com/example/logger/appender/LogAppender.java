package com.example.logger.appender;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.UnsynchronizedAppenderBase;
import com.example.logger.entity.ErrorEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.Getter;
import lombok.Setter;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.Dsl;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.*;
import java.time.LocalDateTime;
import java.util.Arrays;

@Getter
@Setter
public class LogAppender extends UnsynchronizedAppenderBase<ILoggingEvent> {

    /*
    * injected from config file (logback-appender)
    */
    protected String url;
    protected String method;
    protected String service;
    protected String contentType;

    private String server;
    protected String version;

    private ObjectMapper objectMapper = new ObjectMapper();
    private AsyncHttpClient asyncHttpClient = Dsl.asyncHttpClient();


    @Override
    public void start() {
        normalizeMethodName();
        registerLocalDateTime();
        try {
            server = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
        }
        super.start();
    }

    @Override
    public void stop() {
        try {
            asyncHttpClient.close();
        } catch (IOException e) {
        }
        super.stop();
    }

    @Override
    protected void append(ILoggingEvent event) {
        ErrorEvent errorEvent = createErrorEvent(event);
        createIssue(errorEvent);
    }

    private ErrorEvent createErrorEvent(ILoggingEvent event) {
        ErrorEvent errorEvent = new ErrorEvent();
        errorEvent.setEvent_at(LocalDateTime.now());
        errorEvent.setLevel(event.getLevel().levelStr);
        errorEvent.setService(service);
        errorEvent.setServer(server);
        errorEvent.setVersion(version);
        errorEvent.setMessage(event.getFormattedMessage());

        StackTraceElement e = event.getCallerData()[0];
        errorEvent.setError_class(e.getClassName());
        errorEvent.setError_method(e.getMethodName());
        errorEvent.setError_file_name(e.getFileName());
        errorEvent.setError_line(e.getLineNumber());

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
//        errorEvent.setRequest_body(request.getMethod().equalsIgnoreCase("POST") ? request. : null);
        errorEvent.setRequest_url(request.getRequestURL().toString());
        errorEvent.setHttp_method(request.getMethod());
        errorEvent.setPath(request.getRequestURI());
        errorEvent.setUser_agent(request.getHeader("User-Agent"));
        errorEvent.setReferrer(request.getHeader("Referrer"));

        String queryString = request.getQueryString();
        errorEvent.setPath(request.getRequestURI() + ((queryString == null || queryString.isEmpty()) ? "" : "?" + queryString));

        StringBuilder sb = new StringBuilder();
        Arrays.stream(event.getCallerData()).forEach(
                caller -> sb.append('\t').append(caller.toString()).append('\n')
        );
        sb.deleteCharAt(0);
        errorEvent.setStack_trace(sb.toString());

        System.out.println(errorEvent);
        return errorEvent;
    }

    public void createIssue(ErrorEvent event) {
        try {
            asyncHttpClient.preparePost(url)
                    .addHeader("Content-Type", "application/json")
                    .setBody(objectMapper.writeValueAsBytes(event))
                    .execute().get();
        } catch (Exception e) {
            addError("Exception", e);
            return;
        }
    }

    private void normalizeMethodName() {
        method = method.toUpperCase();
    }

    private void registerLocalDateTime() {
        objectMapper.registerModule(new JavaTimeModule());
    }
}